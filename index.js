document.getElementById("btn-bai-1").addEventListener("click", function () {
    console.log("bài 1 ");
    var contentSoChan = "";
    var contentSole = "";
    for (var index = 0; index < 100; index++) {
        if (index % 2 == 0) {
            contentSoChan = contentSoChan + " " + index;
        } else {
            contentSole = contentSole + " " + index;
        }
    }
    document.getElementById("ket-qua").innerHTML = `${contentSoChan}<br><br>${contentSole}`
})
//bài 2
document.getElementById("btn-bai-2").addEventListener("click", function () {
    var n = 0;
    for (var sum = 0; sum < 10000; sum++) {
        sum = sum + n;
        n++;
    }
    console.log(n);
    document.getElementById("ket-qua-bai-2").innerHTML = `<div>Số nguyên dương nhỏ nhất là ${n}</div>`
})
//bài 3
document.getElementById("btn-bai-3").addEventListener("click", function () {
    var soX = document.getElementById("txt-so-x").value * 1;
    var soN = document.getElementById("txt-so-n").value * 1;
    for (var i = 0; i < soN; i++) {
        sum = soX + (Math.pow(soX, i)) + (Math.pow(soX, soN))
    }
    console.log(sum);
    document.getElementById("ket-qua-bai-3").innerHTML = `<div>Tổng là ${sum}</div>`

})

document.getElementById("btn-bai-4").addEventListener("click", function () {
    var soGiaithua = document.getElementById("txt-giai-thua").value * 1
    var sum = 1;
    if (soGiaithua < 0) {
        document.getElementById("ket-qua-bai-4").innerHTML = `<p>Không tính đc</p>`
    } else {
        for (var i = 2; i <= soGiaithua; i++) {
            sum = sum * i;
            console.log(i, sum);
        }
    }
    document.getElementById("ket-qua-bai-4").innerHTML = `<p>${sum}</p>`

})
document.getElementById("btn-bai-5").addEventListener("click", function () {
    var theChan = `<div class="bg-danger">Thẻ chẵn</div>`;
    var theLe = `<div class="bg-primary">Thẻ lẻ</div>`;
    var content = "";
    var doiMau = function (i) {
        if (i % 2 == 0) {

            return theChan;
        } else {

            return theLe;
        }
    }
    for (var i = 0; i < 10; i++) {
        content = content + doiMau(i);
    }
    document.querySelector(".the-bai5").innerHTML = `${content}`;

})